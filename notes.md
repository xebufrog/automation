# Packages
https://github.com/mysqljs/mysql#pooling-connections
https://www.npmjs.com/package/tunnel-ssh

https://www.ssh.com/ssh/tunneling/example


https://medium.com/@jacobvarner14/lessons-learned-from-my-first-four-react-projects-f39f94eca220
https://blog.logrocket.com/javascript-es6-5-new-abstractions-to-improve-your-code-54a369e82407
https://www.codementor.io/carsoncgibbons/quickstart-a-vue-js-store-locator-app-using-a-headless-cms-api-lzvckb6a6
https://medium.com/open-graphql/graphql-bindings-for-service-to-service-communication-d1e89df66ecd

https://codeburst.io/react-authentication-with-twitter-google-facebook-and-github-862d59583105

https://davidwalsh.name/array-destructuring

https://medium.com/@hernanrajchert/creating-typings-for-curry-using-ts-3-x-956da2780bbf
https://medium.com/open-graphql/implementing-search-in-graphql-11d5f71f179

https://stackoverflow.com/questions/11592033/regex-match-text-between-tags

https://medium.com/ft-product-technology/this-one-line-of-javascript-made-ft-com-10-times-slower-5afb02bfd93f

https://medium.com/dailyjs/introducing-spaceace-a-new-kind-of-front-end-state-library-5215b18adc11

https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md

# Testing resourses
https://flaviocopes.com/jest/#create-the-first-jest-test
https://medium.freecodecamp.org/how-to-mock-requests-for-unit-testing-in-node-bb5d7865814a
https://hackernoon.com/how-to-mock-data-for-node-js-applications-using-faker-js-b1f4c0e78102
https://www.davidbaumgold.com/tutorials/
https://www.davidbaumgold.com/tutorials/automated-tests-node/
https://www.davidbaumgold.com/tutorials/host-static-site-aws-s3-cloudfront/

https://blog.newrelic.com/engineering/docker-osx-mac/


# Frontend Masters
https://github.com/kentcdodds/testing-workshop#topics-covered

# Mac Set-up

https://github.com/ptb/mac-setup


https://blog.trendmicro.com/clean-install-macos-high-sierra-includes-installing-macos-usb-device/
https://github.com/mas-cli/mas
https://github.com/herrbischoff/awesome-macos-command-line#google
https://gist.github.com/tylerwalts/9375263
https://pawelgrzybek.com/change-macos-user-preferences-via-command-line/
https://github.com/mathiasbynens/dotfiles/blob/master/.macos
https://wilsonmar.github.io/dotfiles/#trackpad-mouse-keyboard-bluetooth-accessories-and-input
https://github.com/necolas/dotfiles
https://github.com/joeyhoer/starter
https://codepad.co/snippet/v5lCBcXj
https://superuser.com/questions/851839/how-do-i-add-the-week-of-year-to-the-menu-bar-datetime-in-osx-yosemite
https://www.learningosx.com/101-ways-to-tweak-os-x-using-terminal/

https://www.intego.com/mac-security-blog/unlock-the-macos-docks-hidden-secrets-in-terminal/
https://ss64.com/osx/defaults.html
https://gist.github.com/somebox/6b00f47451956c1af6b4
https://github.com/bradp/dotfiles/blob/master/misc/setup-new-mac.sh
http://osxdaily.com/2014/06/19/enable-dark-mode-os-x-yosemite-beta-defaults/
https://medium.com/wearetheledger/oh-my-zsh-made-for-cli-lovers-installation-guide-3131ca5491fb
https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#git
https://github.com/powerline/fonts
https://sourabhbajaj.com/mac-setup/

http://osxdaily.com/2014/01/31/turn-on-mac-touch-to-click-command-line/

https://docs.docker.com/docker-for-mac/
https://pilsniak.com/page/2/
https://www.howtogeek.com/211541/homebrew-for-os-x-easily-installs-desktop-apps-and-terminal-utilities/

https://gist.github.com/nickytonline/8086319bf5836797ee3dea802a77000d
https://www.iamdeveloper.com/my-mac-setup/
https://dev.to/nickytonline/my-visual-studio-code-setup-2ima

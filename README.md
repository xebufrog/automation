# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Auto provision examples for Mac OS X
* Version 0.0.0


### Caveats ###

Applications by default are installed in /opt/homebrew/caskroom/ and symlinked to Applications directory. 

### References ###


[Hacker's Guide to Setting up Your Mac] (http://lapwinglabs.com/blog/hacker-guide-to-setting-up-your-mac/ "Hacker's Guide to Setting up Your Mac")

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact